use quicksort::quicksort;
use rand::prelude::*;

fn main() {
    let mut rng = rand::thread_rng();

    let mut array: Vec<i32> = (0..25).collect();
    array.shuffle(&mut rng);

    println!("Initial array: {:?}", array);
    quicksort(&mut array);
    println!("Sorted array: {:?}", array);
}
