pub fn quicksort<T: PartialOrd + Clone>(array: &mut [T]) {
    if array.len() > 1 {
        let pivot = partition(array).unwrap();
        let (left, right) = array.split_at_mut(pivot);
        quicksort(left);
        quicksort(right);
    }
}

pub fn partition<T: PartialOrd + Clone>(array: &mut [T]) -> Option<usize> {
    let pivot = array.last()?.clone();
    let mut low_index = 0;
    for high_index in 0..array.len() {
        if array[high_index] < pivot {
            array.swap(low_index, high_index);
            low_index += 1;
        }
    }
    array.swap(low_index, array.len() - 1);
    Some(low_index)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_quicksort_empty_array() {
        let mut array: Vec<i32> = vec![];
        quicksort(&mut array);
        assert_eq!(array, vec![]);
    }

    #[test]
    fn test_quicksort_one_element() {
        let mut array = vec![1];
        quicksort(&mut array);
        assert_eq!(array, vec![1]);
    }

    #[test]
    fn test_quicksort_two_elements_sorted() {
        let mut array = vec![1, 2];
        quicksort(&mut array);
        assert_eq!(array, vec![1, 2]);
    }

    #[test]
    fn test_quicksort_two_elements_unsorted() {
        let mut array = vec![2, 1];
        quicksort(&mut array);
        assert_eq!(array, vec![1, 2]);
    }

    #[test]
    fn test_quicksort_standard_elements_unsorted() {
        let mut array = vec![9, 1, 3, 0, 7, 8, 4, 2, 6, 5];
        quicksort(&mut array);
        assert_eq!(array, vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
    }

    #[test]
    fn test_quicksort_partial_sort() {
        let mut array = vec![9, 1, 3, 0, 7, 8, 4, 2, 6, 5];
        quicksort(&mut array[0..5]);
        assert_eq!(array, vec![0, 1, 3, 7, 9, 8, 4, 2, 6, 5]);
    }

    #[test]
    fn test_partition_empty_array() {
        let mut array: Vec<i32> = vec![];

        assert_eq!(partition(&mut array), None);
    }

    #[test]
    fn test_partition_one_element() {
        let mut array = vec![1];

        assert_eq!(partition(&mut array), Some(0));
        assert_eq!(array, vec![1]);
    }

    #[test]
    fn test_partition_two_elements_sorted() {
        let mut array = vec![1, 2];

        assert_eq!(partition(&mut array), Some(1));
        assert_eq!(array, vec![1, 2]);
    }

    #[test]
    fn test_partition_two_elements_unsorted() {
        let mut array = vec![2, 1];

        assert_eq!(partition(&mut array), Some(0));
        assert_eq!(array, vec![1, 2]);
    }

    #[test]
    fn test_partition_standard_elements_unsorted() {
        let mut array = vec![9, 1, 3, 0, 7, 8, 4, 2, 6, 5];
        let pivot_point = partition(&mut array).unwrap();
        let pivot = array[pivot_point];

        assert_eq!(pivot_point, 5);
        assert!(
            array[0..pivot].iter().all(|&value| value <= pivot),
            "All elements before the pivot must be lesser than the pivot"
        );
        assert!(
            array[pivot + 1..array.len()]
                .iter()
                .all(|&value| pivot <= value),
            "All elements after the pivot must be greater than the pivot"
        );
    }
}
