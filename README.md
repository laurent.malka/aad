# Algorithms

## Requirements

In order to run all the following projects, you have to have the **Rust** compiler installed along with **cargo**, the **Rust** dependencies manager and build system.

<https://www.rust-lang.org/learn/get-started>

## A* algorithm

### Usage

You can run the project using the following command

```bash
cd a_star
cargo run
```

A window will open, showing a grid. The starting cell will be displayed in green, the end cell in red and the current cell in blue. In order to move the current cell, you have to hold the right arrow key to go along the path that was found using the **A\* Algorithm**, and the left arrow key to go backwards.

The grid is defined in the `main.rs` file and can easily be modified to generate a new path, but the program then needs to be recompiled using `cargo run`.

The algorithm can be read in the `lib.rs` file.

## Quicksort

### Usage

```bash
cd quicksort
cargo run # Sorts a shuffled array of distinct values from 0 to 24
cargo test # Runs unit tests on the quicksort and partition functions
```

The algorithm and the unit tests can be found in the `lib.rs` file.

### Principle

The `quicksort` algorithm is a non stable sorting algorithm, meaning that equal elements might be reordered.

The algorithm works in the following way:

* An element of the array is chosen to be the pivot (here, the last element is used)
* All the elements that are lesser than the pivot are put before the pivot, andthe others after the pivot
* The array is then split, and the quicksort function is called on the sub-arraythat contains all the elements before the pivot, and on the sub-array thatcontains all the elements after the pivot
* The algorithm does nothing if the array is empty or has only one element
