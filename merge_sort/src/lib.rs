pub fn merge_sort<T: PartialOrd + Clone>(array: &mut [T]) {
    let len = array.len();
    if len > 1 {
        let (left, right) = array.split_at_mut(len / 2);
        merge_sort(left);
        merge_sort(right);

        merge(left.to_vec(), right.to_vec(), array);
    }
}

pub fn merge<T: PartialOrd>(left: Vec<T>, right: Vec<T>, output: &mut [T]) {
    let left_len = left.len();
    let right_len = right.len();

    assert_eq!(left_len + right_len, output.len());

    let mut left_iterator = left.into_iter().peekable();
    let mut right_iterator = right.into_iter().peekable();

    let mut output_iterator = output.iter_mut();

    while left_iterator.peek().is_some() && right_iterator.peek().is_some() {
        if left_iterator.peek() <= right_iterator.peek() {
            *output_iterator.next().unwrap() = left_iterator.next().unwrap();
        } else {
            *output_iterator.next().unwrap() = right_iterator.next().unwrap();
        }
    }

    for remaining_left_value in left_iterator {
        *output_iterator.next().unwrap() = remaining_left_value;
    }

    for remaining_right_value in right_iterator {
        *output_iterator.next().unwrap() = remaining_right_value;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_merge_sort_empty_array() {
        let mut array: Vec<i32> = vec![];
        merge_sort(&mut array);
        assert_eq!(array, vec![]);
    }

    #[test]
    fn test_merge_sort_one_element() {
        let mut array = vec![1];
        merge_sort(&mut array);
        assert_eq!(array, vec![1]);
    }

    #[test]
    fn test_merge_sort_two_elements_sorted() {
        let mut array = vec![1, 2];
        merge_sort(&mut array);
        assert_eq!(array, vec![1, 2]);
    }

    #[test]
    fn test_merge_sort_two_elements_unsorted() {
        let mut array = vec![2, 1];
        merge_sort(&mut array);
        assert_eq!(array, vec![1, 2]);
    }

    #[test]
    fn test_merge_sort_standard_elements_unsorted() {
        let mut array = vec![9, 1, 3, 0, 7, 8, 4, 2, 6, 5];
        merge_sort(&mut array);
        assert_eq!(array, vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
    }

    #[test]
    fn test_merge_sort_partial_sort() {
        let mut array = vec![9, 1, 3, 0, 7, 8, 4, 2, 6, 5];
        merge_sort(&mut array[0..5]);
        assert_eq!(array, vec![0, 1, 3, 7, 9, 8, 4, 2, 6, 5]);
    }

    #[test]
    fn test_merge_empty_arrays() {
        let mut array: Vec<i32> = vec![];
        merge(vec![], vec![], &mut array);

        assert_eq!(array, vec![]);
    }

    #[test]
    fn test_merge_empty_and_one_element_array() {
        let mut array: Vec<i32> = vec![0];
        merge(vec![1], vec![], &mut array);

        assert_eq!(array, vec![1]);
    }

    #[test]
    fn test_merge_one_element_arrays_sorted() {
        let mut array: Vec<i32> = vec![0, 0];
        merge(vec![1], vec![2], &mut array);

        assert_eq!(array, vec![1, 2]);
    }

    #[test]
    fn test_merge_one_element_arrays_unsorted() {
        let mut array: Vec<i32> = vec![0, 0];
        merge(vec![2], vec![1], &mut array);

        assert_eq!(array, vec![1, 2]);
    }

    #[test]
    fn test_merge_standard_array() {
        let mut array: Vec<i32> = vec![0, 0, 0, 0, 0, 0, 0, 0, 0];
        merge(vec![2, 4, 6, 8, 10], vec![1, 3, 7, 15], &mut array);

        assert_eq!(array, vec![1, 2, 3, 4, 6, 7, 8, 10, 15]);
    }
}
