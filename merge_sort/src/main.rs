use merge_sort::merge_sort;
use rand::prelude::*;

fn main() {
    let mut rng = rand::thread_rng();

    let mut array: Vec<i32> = (0..25).collect();
    array.shuffle(&mut rng);

    println!("Initial array: {:?}", array);
    merge_sort(&mut array);
    println!("Sorted array: {:?}", array);
}
