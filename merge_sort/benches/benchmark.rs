use merge_sort::merge_sort;

use criterion::{criterion_group, criterion_main, Criterion};
use rand::prelude::*;
use rand::rngs::StdRng;

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut rng = StdRng::seed_from_u64(0u64);
    let mut output: Vec<i32> = (0..10000).map(|_| rng.gen()).collect();
    c.bench_function("merge_sort sync", |b| b.iter(|| merge_sort(&mut output)));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
