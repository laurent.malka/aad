pub mod gui;

use std::collections::HashSet;

type Cell = (i32, i32);

enum VisitState {
    Visited,
    Unvisited,
}

struct CellData {
    f: f64,
    h: f64,
    g: f64,
    parent: Option<Cell>,
    visit_state: VisitState,
}

#[derive(Clone, PartialEq)]
pub enum CellType {
    Empty,
    Wall,
}

pub struct AStar<'a> {
    grid: &'a [&'a [CellType]],
    cells_data: Vec<Vec<CellData>>,
    start_cell: &'a Cell,
    end_cell: &'a Cell,
}

impl<'a> AStar<'a> {
    pub fn new(
        grid: &'a [&'a [CellType]],
        start_cell: &'a Cell,
        end_cell: &'a Cell,
    ) -> Result<Self, String> {
        if grid.len() == 0 || grid[0].len() == 0 {
            return Err("Grid cannot be flat".into());
        }

        let cells_data = Self::init_cells_data(grid, start_cell);

        let result = AStar {
            grid,
            cells_data,
            start_cell,
            end_cell,
        };

        if !result.is_valid(start_cell) {
            Err("Start cell not in grid".into())
        } else if !result.is_valid(end_cell) {
            Err("End cell not in grid".into())
        } else if result.is_wall(start_cell) {
            Err("Start cell is on a wall".into())
        } else if result.is_wall(end_cell) {
            Err("End cell is on a wall".into())
        } else {
            Ok(result)
        }
    }

    fn init_cells_data(grid: &'a [&'a [CellType]], start_cell: &'a Cell) -> Vec<Vec<CellData>> {
        let mut result = Vec::with_capacity(grid.len());
        for _row_number in 0..grid.len() {
            let mut row = Vec::with_capacity(grid[0].len());
            for _col_number in 0..grid[0].len() {
                row.push(CellData {
                    f: std::f64::INFINITY,
                    g: std::f64::INFINITY,
                    h: std::f64::INFINITY,
                    parent: None,
                    visit_state: VisitState::Unvisited,
                });
            }
            result.push(row);
        }

        let mut start_cell_data = &mut result[start_cell.1 as usize][start_cell.0 as usize];
        start_cell_data.f = 0.0;
        start_cell_data.g = 0.0;
        start_cell_data.h = 0.0;

        result
    }

    fn get_cell_data(&self, cell: &Cell) -> &CellData {
        &self.cells_data[cell.1 as usize][cell.0 as usize]
    }

    fn get_cell_data_mut(&mut self, cell: &Cell) -> &mut CellData {
        &mut self.cells_data[cell.1 as usize][cell.0 as usize]
    }

    pub fn run(&mut self) -> Result<Vec<Cell>, ()> {
        let mut cells_to_visit: HashSet<Cell> = HashSet::new();
        cells_to_visit.insert(self.start_cell.clone());

        'main_loop: while !cells_to_visit.is_empty() {
            let current_cell = cells_to_visit
                .iter()
                .min_by(|cell_a, cell_b| {
                    let cell_a_f = self.get_cell_data(cell_a).f;
                    let cell_b_f = self.get_cell_data(cell_b).f;
                    cell_a_f.partial_cmp(&cell_b_f).unwrap()
                })
                .cloned()
                .unwrap();
            cells_to_visit.remove(&current_cell);

            let mut cell_data = self.get_cell_data_mut(&current_cell);
            cell_data.visit_state = VisitState::Visited;

            let next_cells = [
                (current_cell.0, current_cell.1 - 1),
                (current_cell.0, current_cell.1 + 1),
                (current_cell.0 - 1, current_cell.1),
                (current_cell.0 + 1, current_cell.1),
            ];

            for next_cell in next_cells.into_iter() {
                if !self.is_valid(next_cell) || self.is_wall(next_cell) {
                    continue;
                }
                if next_cell == self.end_cell {
                    self.get_cell_data_mut(next_cell).parent = Some(current_cell.clone());

                    break 'main_loop;
                }

                let new_g = self.get_cell_data(&current_cell).g + 1.0;
                let new_h = self.compute_h(&next_cell, &self.end_cell);
                let new_f = new_g + new_h;
                if new_f < self.get_cell_data(next_cell).f {
                    cells_to_visit.insert(*next_cell);

                    let next_cell_data = self.get_cell_data_mut(next_cell);

                    next_cell_data.g = new_g;
                    next_cell_data.h = new_h;
                    next_cell_data.f = new_f;
                    next_cell_data.parent = Some(current_cell.clone());
                }
            }
        }

        if self.get_cell_data(self.end_cell).parent.is_some() {
            let mut stack = vec![self.end_cell.clone()];

            let mut current_cell = self.end_cell.clone();
            while let Some(parent) = self.get_cell_data(&current_cell).parent {
                stack.push(parent.clone());
                current_cell = parent;
            }

            Ok(stack.into_iter().rev().collect())
        } else {
            Err(())
        }
    }

    fn is_valid(&self, cell: &Cell) -> bool {
        cell.1 >= 0
            && (cell.1 as usize) < self.grid.len()
            && cell.0 >= 0
            && (cell.0 as usize) < self.grid[0].len()
    }

    fn is_wall(&self, cell: &Cell) -> bool {
        self.grid[cell.1 as usize][cell.0 as usize] == CellType::Wall
    }

    fn compute_h(&self, cell_1: &Cell, cell_2: &Cell) -> f64 {
        (cell_1.0 - cell_2.0).abs() as f64 + (cell_1.1 - cell_2.1).abs() as f64
    }
}
