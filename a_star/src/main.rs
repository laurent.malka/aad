use a_star::{gui::*, AStar, CellType};
use ggez::event;

fn main() {
    const WIDTH: usize = 10;
    const HEIGHT: usize = 9;
    #[rustfmt::skip]
    const GRID: &[&[CellType]] = &[
        &[CellType::Empty, CellType::Wall, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Empty],
        &[CellType::Empty, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Empty],
        &[CellType::Empty, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Wall, CellType::Empty],
        &[CellType::Wall, CellType::Wall, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Wall, CellType::Wall, CellType::Wall, CellType::Wall, CellType::Empty],
        &[CellType::Empty, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Wall],
        &[CellType::Empty, CellType::Wall, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Wall, CellType::Wall],
        &[CellType::Empty, CellType::Wall, CellType::Wall, CellType::Wall, CellType::Wall, CellType::Empty, CellType::Wall, CellType::Wall, CellType::Wall, CellType::Empty],
        &[CellType::Empty, CellType::Wall, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Empty, CellType::Empty, CellType::Empty],
        &[CellType::Empty, CellType::Empty, CellType::Empty, CellType::Wall, CellType::Wall, CellType::Wall, CellType::Empty, CellType::Wall, CellType::Wall, CellType::Empty],
    ];

    let start_cell = (0, 8);
    let end_cell = (9, 3);
    
    let mut a_star = AStar::new(&GRID, &start_cell, &end_cell).expect("Invalid grid or cells");
    let path = a_star.run();

    if path.is_err() {
        println!("Could not find a path");
        return;
    }

    let mut state = State::new(
        GRID.iter()
            .map(|row| row.iter().cloned().collect())
            .collect(),
        (0, 8),
        (9, 3),
        path.unwrap(),
    );
    let cb = ggez::ContextBuilder::new("A* algorithm", "Laurent Malka")
        .window_mode(
            ggez::conf::WindowMode::default().dimensions(WIDTH as f32 * 50.0, HEIGHT as f32 * 50.0),
        )
        .window_setup(ggez::conf::WindowSetup::default().title("A* algorithm"));
    let (ref mut ctx, ref mut event_loop) = &mut cb.build().unwrap();
    event::run(ctx, event_loop, &mut state).unwrap();
}
