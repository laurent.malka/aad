use super::{Cell, CellType};
use ggez::event::KeyCode;
use ggez::input::keyboard;
use ggez::*;

type Grid = Vec<Vec<CellType>>;

pub struct State {
    grid: Grid,
    start_cell: Cell,
    end_cell: Cell,
    path: Vec<Cell>,
    path_index: usize,
}

impl State {
    pub fn new(grid: Grid, start_cell: Cell, end_cell: Cell, path: Vec<Cell>) -> State {
        State {
            grid,
            start_cell,
            end_cell,
            path,
            path_index: 0,
        }
    }
}

impl ggez::event::EventHandler for State {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        if keyboard::is_key_pressed(ctx, KeyCode::Right) && self.path_index + 1 < self.path.len() {
            self.path_index += 1;
        } else if keyboard::is_key_pressed(ctx, KeyCode::Left) && self.path_index != 0 {
            self.path_index -= 1;
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        for (row_number, row) in self.grid.iter().enumerate() {
            for (col_number, cell) in row.iter().enumerate() {
                let color = match (col_number, row_number, cell) {
                    (x, y, _)
                        if x == self.path[self.path_index].0 as usize
                            && y == self.path[self.path_index].1 as usize =>
                    {
                        graphics::Color::from_rgb(0, 0, 255)
                    }
                    (x, y, _)
                        if x == self.start_cell.0 as usize && y == self.start_cell.1 as usize =>
                    {
                        graphics::Color::from_rgb(0, 255, 0)
                    }
                    (x, y, _) if x == self.end_cell.0 as usize && y == self.end_cell.1 as usize => {
                        graphics::Color::from_rgb(255, 0, 0)
                    }
                    (_, _, CellType::Empty) => graphics::WHITE,
                    (_, _, CellType::Wall) => graphics::BLACK,
                };
                let rect = ggez::graphics::Mesh::new_rectangle(
                    ctx,
                    ggez::graphics::DrawMode::fill(),
                    graphics::Rect::new(
                        col_number as f32 * 50.0,
                        row_number as f32 * 50.0,
                        50.0,
                        50.0,
                    ),
                    color,
                )?;
                graphics::draw(ctx, &rect, graphics::DrawParam::default())?;
            }
        }
        graphics::present(ctx)?;
        Ok(())
    }
}
